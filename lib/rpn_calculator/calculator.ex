defmodule RpnCalculator.Calculator do
  use GenServer
  @timeout 60 * 1000  * 5

  #######################################################################
  ### Client API
  def start_link(start_val, name, timeout) do
    GenServer.start_link(__MODULE__, [start: start_val, timeout: timeout], [name: name])
  end

  @doc """
  Starts a new calcuator process
  """
  def on(name, opts \\ []) do
    opts = Map.merge(%{timeout: @timeout}, Enum.into(opts, %{}))
    Supervisor.start_child(RpnCalculator.Supervisor, [name, opts.timeout])
  end

  @doc """
  Gets the current value in calculator memory
  """
  def current(pid) do
    GenServer.call(pid, :current)
  end

  @doc """
  Clears the calculator memory
  """
  def clear(pid) do
    GenServer.call(pid, :clear)
  end

  @doc """
  Sets a new memory value on the Calculator
  """
  def current(pid, val) when is_integer(val) do
    GenServer.call(pid, {:set_current, val})
  end

  def current(pid, val) when is_float(val) do
    GenServer.call(pid, {:set_current, val})
  end

  @doc """
  Turn the calculator off which clears the memory and ends the process
  """
  def off(pid) do
    GenServer.stop(pid)
  end

  @doc """
  Adds numbers to the calculator
  """
  def add(pid, numbers) when is_list(numbers) do
    add_call(pid, numbers)
  end

  def add(pid, number) when is_integer(number) or is_float(number) do
    add_call(pid, [ number ])
  end

  defp add_call(pid, numbers) do
    GenServer.call(pid, {:add, numbers})
  end

  @doc """
  Subtracts values from the calculator
  """
  def subtract(pid, numbers) when is_list(numbers) do
    subtract_call(pid, numbers)
  end

  def subtract(pid, number) when is_integer(number) or is_float(number) do
    subtract_call(pid, [ number ])
  end

  defp subtract_call(pid, numbers) do
    GenServer.call(pid, {:subtract, numbers})
  end

  #######################################################################
  ### Server API
  def init([start: start, timeout: timeout]) do
    {:ok, %{current: start, timeout: timeout}, timeout}
  end

  def handle_call({:set_current, val}, _from, state) do
    {:reply, val, %{state | current: val}, state.timeout}
  end

  def handle_call(:clear, _from, state) do
    IO.puts "Clearing calculator memory"
    {:reply, 0 , %{state | current: 0}, state.timeout}
  end

  def handle_call({:add, numbers}, _from, %{current: current} = state) do
    new_current = (current + Enum.sum(numbers))
    {:reply, new_current, %{state | current: new_current}, state.timeout}
  end

  def handle_call({:subtract, numbers}, _from, %{current: current} = state) do
    new_current = (current - Enum.sum(numbers))
    {:reply, new_current, %{state | current: new_current}, state.timeout}
  end

  def handle_call(:current, _from, state) do
    {:reply, state.current, state, state.timeout}
  end

  def handle_info(:timeout, state) do
    {:stop, :normal, state}
  end
end
