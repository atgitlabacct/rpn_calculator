defmodule RpnCalculatorTest do
  use ExUnit.Case, async: true
  doctest RpnCalculator

  alias RpnCalculator.Calculator

  test "can start and stop a calculator" do
    {:ok, pid} = Calculator.on(:new_calc)
    assert Process.alive?(pid)

    Calculator.off(:new_calc)
    assert Process.alive?(pid) == false
  end

  test "we can set a new start value" do
    {:ok, pid} = Calculator.on(:new_calc)
    assert 10 = Calculator.current(pid, 10)
    assert 10 = Calculator.current(pid)
  end

  test "turns off after certain period" do
    {:ok, pid} = Calculator.on(:new_calc, timeout: 300)
    :timer.sleep(500)
    assert Process.alive?(pid) == false
  end

  describe "adding numbers" do
    test "via a list" do
      {:ok, pid} = Calculator.on(:new_calc)
      result = Calculator.add(pid, [1.1,2,3])
      assert result == 6.1
      assert Calculator.current(pid) == 6.1
    end

    test "via a single number" do
      {:ok, _pid} = Calculator.on(:new_calc)
      result = Calculator.add(:new_calc, 2.2)
      assert result == 2.2
      result = Calculator.add(:new_calc, 1)
      assert result == 3.2
      assert Calculator.current(:new_calc) == 3.2
    end
  end

  describe "subtracting numbers" do
    test "via a list" do
      {:ok, _pid} = Calculator.on(:new_calc)
      Calculator.current(:new_calc, 10)
      result = Calculator.subtract(:new_calc, [1,2,3])
      assert result == 4
      assert Calculator.current(:new_calc) == 4
    end

    test "via a single number" do
      {:ok, _pid} = Calculator.on(:new_calc)
      Calculator.current(:new_calc, 10)
      result = Calculator.subtract(:new_calc, 3)
      assert result == 7
      result = Calculator.subtract(:new_calc, 3.5)
      assert result == 3.5
      assert Calculator.current(:new_calc) == 3.5
    end
  end

  test "divide numbers" do
    {:ok, _pid} = Calculator.on
    result = Calculator.divide(100, [5, 2])
    assert result == 10

    # Calculator with prev value
    result = Calculator.divide(2)
    assert result == 5
    result = Calculator.divide([5,1])
    assert result == 1
  end

  test "multiply numbers" do
    {:ok, _pid} = Calculator.on
    result = Calculator.multiply(10, [2, 2])
    assert result == 40

    # Calculator with prev value
    result = Calculator.multiply(2)
    assert result == 80
    result = Calculator.multiply([2,2])
    assert result == 320
  end
end
